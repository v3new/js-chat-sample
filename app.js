"use strict";

var http = require("http");
var webserver = require("node-static");
var ws = new require("ws");

const SERVER_HOST = "localhost";
const SERVER_PORT = 8088;
const CLIENT_PORT = 8080;

var server = new ws.Server({port: SERVER_PORT});
var client = new webserver.Server("./public");

// users and history of all messages
var clients = [];
var history = [];

// function: send broadcast message for all user
var sendBroadcastMessage = function(data) {
  var content = JSON.stringify({ data });
  for (var id in clients) {
    clients[id].ws.send(content);
  }
};

// function: send message for just one user
var sendMessage = function(data, user) {
  var content = JSON.stringify({ data });
  clients[user].ws.send(content);
};

// function: send list of users in online to all users
var refreshOnline = function() {
  var onlineList = [];

  for (var key in clients)
    onlineList.push(clients[key].userName)

  var command = {
    type: "command",
    text: "online",
    list: onlineList
  };

  sendBroadcastMessage(command);
};

// function: for escaping input strings
function htmlEntities(str) {
  return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .trim();
}

server.on("connection", function(conn) {

  var id = Math.random();
  clients[id] = {};
  clients[id].ws = conn;

  // connect USER_ID from IP_ADDRESS
  console.log("connect", id, "from",
               conn.upgradeReq.connection.remoteAddress);

  // if CORS
  if (conn.upgradeReq.headers.origin !=
      "http://" + SERVER_HOST + ":" + CLIENT_PORT) {

    console.log("CSRF attempt", id);

    var record = {
      time: (new Date()).getTime(),
      type: "info",
      text: "kick",
      user: clients[id].userName,
      id: id
    };

    history.push(record);

    // remove user from memory
    delete clients[id];

    // send notify message to users about user exit
    sendBroadcastMessage(record);

    // send list of users in online
    refreshOnline();

  } else {

    var command = {
      type: "command",
      text: "identify"
    };

    // Asks user for enter her nickname
    sendMessage(command, id);

    conn.on("message", function(data) {
      var message = JSON.parse(data);
      var userInput = htmlEntities(message.data);

      switch (message.type) {
        case "nickname":

          console.log("user", id, "attempt change a nickname to",
                      userInput);

          var result = true;

          for (var key in clients)
            if (clients[key].userName == userInput) {
              // if any users already have this nickname
              result = false;
            }

          if (result) {
            clients[id].userName = userInput;
            console.log("successful attempt");

            // prepare welcome command
            var command = {
              type: "command",
              text: "welcome",
              user: id
            }

            // sends all chat history, if her exist
            if (history.length > 0)
              sendMessage(history, id);

            var record = {
              time: (new Date()).getTime(),
              type: "info",
              text: "login",
              user: clients[id].userName,
              id: id
            };

            // adds to chat history record, about user auth
            history.push(record);

            // send notify message to users about new user
            sendBroadcastMessage(record);

            // send list of users in online
            refreshOnline();
          } else {
            console.log("unsuccessful attempt");

            // prepare command for action to retype nickname
            var command = {
              type: "command",
              text: "rename",
              user: id
            }
          }

          // send a command
          sendMessage(command, id);
        break;
        case "message":
          console.log("new message:", userInput, "from",
                      clients[id].userName);

          var record = {
            time: (new Date()).getTime(),
            type: "message",
            text: userInput,
            user: clients[id].userName,
            id: id
          };

          history.push(record);

          // resend message to all users
          sendBroadcastMessage(record);
        break;
        default:
          console.log("some error");
      }
    });

    conn.on("close", function() {
      console.log("logout", id);

      var record = {
        time: (new Date()).getTime(),
        type: "info",
        text: "logout",
        user: clients[id].userName,
        id: id
      };

      history.push(record);

      // remove user from memory
      delete clients[id];

      // send notify message to users about user exit
      sendBroadcastMessage(record);

      // send list of users in online
      refreshOnline();
    });
  }
});

http.createServer(function (req, res) {
  client.serve(req, res);
}).listen(CLIENT_PORT);

console.log("client port", CLIENT_PORT, "server port", SERVER_PORT);
