"use strict";

const SERVER_HOST = "localhost";
const SERVER_PORT = 8088;

var currentUserName;

if (!window.WebSocket) {
  document.getElementById('flow').insertAdjacentHTML(
    "afterbegin", "<div><span class=\"info\">" +
    "<b>WebSocket в этом браузере не поддерживается.</b>" +
    "</span></div>");
}

var socket = new WebSocket("ws://" + SERVER_HOST + ":" + SERVER_PORT);

var notify = new Audio();
notify.src = './notify.mp3';

socket.onopen = function() {
  console.log("connection is ok");
};

socket.onclose = function(event) {
  if (event.wasClean) {
    var status = "Соединение закрыто";
  } else {
    var status = "Обрыв соединения";
  }
  document.getElementById('flow').insertAdjacentHTML(
    "afterbegin", "<div><span class=\"info\">" +
    "<b>" + status + "</b> " + "код: " + event.code + "</span></div>");
  document.getElementById("online").style.display = "none";
  document.forms.feed.style.display = "none";
  console.log("connection is close");
};

socket.onmessage = function(event) {
  var item = JSON.parse(event.data);

  if (item.data.length) {
    item.data.forEach(function(data) {
      showMessage(data);
    })
  }else{
    showMessage(item.data);
  }
};

socket.onerror = function(error) {
  document.getElementById('flow').insertAdjacentHTML(
    "afterbegin", "<div><span class=\"info\">" +
    "<b>Ошибка связи:</b> " + error.message + "</span></div>");
  console.log("connection error");
};

document.forms.feed.onsubmit = function() {
  var message =  this.message.value.trim();

  if (message.length > 0) {
    var data = JSON.stringify({ type: "message", data: message });
    socket.send(data);
    this.message.value = "";
  }

  return false;
};

var showMessage = function(message) {
  switch(message.type) {
    case 'command':
      if (message.text == "identify") {
        auth();
      }
      if (message.text == "rename") {
        alert("К сожалению, данный никнейм уже используется другим пользователем, попробуйте другой");
        auth();
      }
      if (message.text == "welcome") {
        document.forms.feed.style.display = "block";
        document.getElementById("online").style.display = "block";
      }
      if (message.text == "online") {
        document.getElementById('online-list').innerHTML = (message.list + "").split(",").join(" ");
      }
    break;
    case 'info':
      if (message.text == "login") {
        document.getElementById('flow').insertAdjacentHTML(
          "afterbegin", "<div><span class=\"info\">" +
          "к чату присоединился <b>" + message.user + "</b></span></div>");
        playRingtone();
      }
      if (message.text == "logout") {
        document.getElementById('flow').insertAdjacentHTML(
          "afterbegin", "<div><span class=\"info\">" +
          "из чата ушел <b>" + message.user + "</b></span></div>");
      }
    break;
    case 'message':
      if (currentUserName == message.user) {
        document.getElementById('flow').insertAdjacentHTML(
          "afterbegin", "<div class=\"right\"><span class=\"message\">" +
           message.text + "</span></div>");
      } else {
        document.getElementById('flow').insertAdjacentHTML(
          "afterbegin", "<div class=\"left\"><span class=\"message\">" +
           "<b>" + message.user + "</b>: " + message.text + "</span></div>");
        playRingtone();
      }
    break;
    default:
      console.log("some error");
  }
}

var auth = function() {
  var defaultUserName = ["Lucky Rabbit", "MyLifeDeBaf", "Allure",
      "ЖдуПятницу", "ЖиВу БеЗ сТрАХа", "E-ron", "ILOJY", "IZY", "KoRmIx",
      "Pozitiv4ik", "TheCrazyVolf", "В ожидании чуда", "The Farrow",
      "Amazar", "Brave Indigo Lobster", "MoRGaN", "TheBlackShark",
      "XEMI", "ПЧеЛк@", "CrazyMan", "Baby boom", "Brolsorro",
      "Stoned Bunny", "Супер€™", "Glamorous", "Xandreads Alliance",
      "АделкаSmaill", "ASIMU", "Cold Storm", "Homeless Puppy", "Jacikey",
      "juni0r", "Lucky Sunshine", "Mel Rose", "NickLost", "Picker",
      "Rusty Villain", "AlphA HeliX", "Atashi", "Bumagorez",
      "Elthins Horde", "evil spirit", "Fendo", "GEJIOR", "KUMO",
      "RUBO", "sacredly", "SexyBaf", "sKaLa", "Straw Oyster", "Streetdrill",
      "The Star", "Transline", "West Tiger", "Wooden Stony Squirrel",
      "Мокетон", "BafLocer", "Beleps Rollers", "Cibrocks Trolls", "El Rat",
      "Freshlux", "Helpless Sapphire", "Honey Alligator", "I7pocTo_HanpocTo",
      "IBudun", "immortal", "Irenes", "Ixils Apotheoses", "JuSTEasy", "Madpig",
      "Marichiano", "NI", "Setbika", "silcon", "Slidy Indigo Honey",
      "Tiny Zebra", "Toncon", "Trendi-brendi", "Vicious Rebel"];

  var userName = prompt("Пожалуйста, укажите никнейм",
      defaultUserName[Math.floor(Math.random() * defaultUserName.length)]);

  if (!userName) {
    auth();
    return false;
  }

  var data = JSON.stringify({ type: 'nickname', data: userName });
  currentUserName = userName;
  socket.send(data);
  return false;
}

var playRingtone = function() {
  notify.currentTime = 0;
  notify.play();
}

var ctrl;

var keyDown = function(event) {
  if (event.keyCode == 17)
		ctrl = true;
  else if(event.keyCode == 13 && ctrl) {
    document.getElementById("submit").click();
  }
}

var keyUp = function(event) {
	if(event.keyCode == 17)
		ctrl = false;
}
